

import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
sock.connect(('127.0.0.1', 6789)) #Endereço de IP e Porta

try:
    
    # Send data/Envio
    message = ""
    message = raw_input("Informe a função desejada: ")
    message += '\n'
    print(message)
    print >>sys.stderr, 'enviando "%s"' % message
    sock.send(message)

    # Look for the response
    amount_received = 0
    amount_expected = len(message)
    
    while amount_received < amount_expected:
        data = sock.recv(16)
        amount_received += len(data)
        print >>sys.stderr, 'received "%s"' % data

finally:
    print >>sys.stderr, 'closing socket'
    sock.close()

