import java.io.*;
import java.net.*;
import javax.swing.*;
/**
* @author: Gleidimar Luiz (Kable Mithus)
*/
public class Cliente {
   public static void main(String args[]) throws Exception{
      String strMsg;
      String strMsgModificada;
      String strIpServidor;
      // Criando socket para o servidor
      strIpServidor = "192.168.9.75";
      Socket socketCliente = new Socket(strIpServidor,6789);
      // Criando stream para enviar mensagem para o servidor
      DataOutputStream msgParaServidor = new
      DataOutputStream(socketCliente.getOutputStream());
      // Lendo mensagem do teclado
      strMsg = JOptionPane.showInputDialog(null,
         "Informe seu palpite:");
      // Enviando mensagem digitada ao servidor
      msgParaServidor.writeBytes(strMsg + "\n");
      // Criando buffer para armazenar mensagem retornada pelo servidor
      BufferedReader msgDoServidor = new BufferedReader(
         new InputStreamReader(socketCliente.getInputStream()));
      // Recebendo mensagem modificada do servidor
      strMsgModificada = msgDoServidor.readLine();
      // Exibindo mensagem modificada
      JOptionPane.showMessageDialog(null,"" + strMsgModificada);
      // Fechando o socket para o servidor
      socketCliente.close();
      System.exit(0);
   }
}