import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Random;


public class Servidor extends Thread{
  static String[] palavrasPossiveis = {"Augustinopolis&14", "Caraguatatuba&13", "Cordeiropolis&13", "Esperantinopolis&16", "Itaquaquecetuba&15", "Paranapiacaba&13", "Constitucionalissimamente&25"};
  static String[] palavraSelecionada;
  static String ipClientes[] = new String[3];
  static int pontuacaoPrimeiro;
  static int pontuacaoSegundo;
  static int pontuacaoTerceiro;

  public static String[] gerarPalavraChave() {
    Random rand = new Random();
    String palavraChave;
    int indexRand =  rand.nextInt(6);
    palavraChave = palavrasPossiveis [indexRand];
    palavraSelecionada = palavraChave.split("&");

    return palavraSelecionada;
  }

  //Compara o palpite enviado pelo client com a resposta
  public static boolean acertou (String palpiteDoCliente, String palavraSelecionada){
    
  if (palpiteDoCliente.equals(palavraSelecionada)){
          return true;
  }else{ 
        return false;
    }
  }



   public static void main(String args []){
      int cont = 0;
      ServerSocket socketServidor = null;
      try {
         // Criando um socket servidor
         socketServidor = new ServerSocket(6789);
         System.out.println(" ________________________________________________________________________");
         System.out.println("|------------------------------------------------------------------------|");
         System.out.println("|-----------------------------Servidor Ativo!----------------------------|");
         System.out.println("|------------------------------------------------------------------------|");
         System.out.println("|---------------------------Aguardando Resposta--------------------------|");
         System.out.println("|------------------------------------------------------------------------|");
         System.out.println("|---------------------------QUANTAS LETRAS TEM---------------------------|");
         System.out.println("|------------------------------------------------------------------------|");
         System.out.println("|------------------------------------------------------------------------|");
         System.out.println("|-Adivinhe quantas letras tem a palavra abaixo no menor tempo possível!-|");
         System.out.println("|------------A pessoa que der o PRIMEIRO palpite ganha 3 pontos----------|");
         System.out.println("|-----------------------------VALENDO!-----------------------------------|");
         System.out.println("|------------------------------------------------------------------------|");
         gerarPalavraChave();
         System.out.println(palavraSelecionada[0]);
         while(true){
            // Aceitando conexao
            Socket conexao = socketServidor.accept();

            //cria uma thread que envia a conexao
            Thread t = new Servidor(conexao, cont);

            cont++;

            //inicia a thread t
            t.start();
                                }
      } catch (IOException e) {
         System.out.println("Erro: " + e);
         System.exit(0);
      }
   }

  private Socket conexao;
  public Servidor(Socket s, int pos){//recebe o valor do socket enviado na thread
    conexao = s;
    ipClientes[pos] = ((conexao.getInetAddress()).toString());
  }

   @Override
   public void run() {
      String mensagemCliente;
      String mensagemClienteModificada;
      try {                   
            // Criando buffer para armazenar a mensagem recebida do cliente
            BufferedReader msgDoCliente = new BufferedReader(new
            InputStreamReader(conexao.getInputStream()));
            // Criando stream para enviar mensagem para o cliente
            DataOutputStream msgParaCliente = new
            DataOutputStream(conexao.getOutputStream());

            msgParaCliente.writeBytes(palavraSelecionada[0]);

            // Lendo mensagem enviada pelo cliente
            mensagemCliente = msgDoCliente.readLine();
            // Exibindo mensagem lida
            System.out.println(conexao.getInetAddress() + ": " + mensagemCliente);
            // Modificando a mensagem lida do cliente
            mensagemClienteModificada = "Palpite Recebido: " + mensagemCliente.toUpperCase() + "\n";
            boolean respostaCerta = acertou(mensagemCliente, palavraSelecionada[1]);
            // Enviando mensagem modificada para o cliente
            //msgParaCliente.writeBytes(mensagemClienteModificada);

            //Contagem de pontos
            if(respostaCerta){
             if(((conexao.getInetAddress()).toString()).equals(ipClientes[0])){
               //incrementa a pontuação do cliente que acertou
               msgParaCliente.writeBytes("1 - PARABENS VOCE ACERTOU, +3 PONTOS" + ((conexao.getInetAddress()).toString()));
               pontuacaoPrimeiro = pontuacaoPrimeiro + 3;
             }
             if(((conexao.getInetAddress()).toString()).equals(ipClientes[1])){
               msgParaCliente.writeBytes("2 - PARABENS VOCE ACERTOU, +3 PONTOS" + ((conexao.getInetAddress()).toString()));
               pontuacaoSegundo = pontuacaoSegundo + 3;
             }
             if(((conexao.getInetAddress()).toString()).equals(ipClientes[2])){
               msgParaCliente.writeBytes("3 - PARABENS VOCE ACERTOU, +3 PONTOS" + ((conexao.getInetAddress()).toString()));
               pontuacaoTerceiro = pontuacaoTerceiro + 3;
             }
 
             //Contagem de pontos
           }else{
            msgParaCliente.writeBytes("QUE PENA, VOCÊ ERROU!");
           }

      } catch (IOException e) {
         System.out.println("Erro: " + e);
         System.exit(0);
      }
      
   }
}