require 'socket'

server = TCPSocket.open('127.0.0.1', 6789) # conecta ao servidor na porta 6789

puts "|-----------------------------------------------------------------------|"
puts "|-Adivinhe quantas letras tem a palavra abaixo no menor tempo possível!-|"
puts "|------------A pessoa que der o PRIMEIRO palpite ganha 3 pontos---------|"
puts "|-----------------------------VALENDO!----------------------------------|"
puts "|-----------------------------teste-------------------------------------|"
puts "|---------------------Quantas letras tem a palavra?---------------------|"

palavra = server.recvfrom( 100000 ) #Recebe Palavra -100000 bytes - do servidor
puts palavra #Imprime a palavra recebida do Servidor

quantidade = gets #Le palpite do usuario via teclado
quantidade = quantidade.chomp 
server.puts quantidade #Envia palpite pro servidor


resp = server.recvfrom( 100000 ) # recebe a resposta -100000 bytes - do servidor
puts resp
gets
server.close # Fecha a conexão com o servidor